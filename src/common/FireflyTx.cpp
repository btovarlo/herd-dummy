#include "dummy/swatch/FireflyTx.hpp"

#include "swatch/core/MetricConditions.hpp"

#include "dummy/OpticalModuleStatus.hpp"
#include "dummy/OpticalTxChannelStatus.hpp"
#include "dummy/OpticsController.hpp"


namespace dummy {
namespace swatch {

using namespace ::swatch;

FireflyTx::FireflyTx(const std::string& aId) :
  OpticalModule(aId, {}, { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 }),
  mController(OpticsController::create(aId, {}, { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 })),
  mPartNumber(registerProperty<std::string>("Part number")),
  mFirmwareVersion(registerProperty<std::string>("Firmware version")),
  mMetricTemperature(registerMetric<float>("temperature", ::swatch::core::GreaterThanCondition<float>(50)))
{
  mMetricTemperature.setUnit("C");
  mMetricTemperature.setFormat(core::format::kFixedPoint, 1);

  setAsPresent();

  for (auto* channel : getOutputs()) {
    mTxEqualization[channel] = &channel->registerProperty<float>("Equalization");
    mTxPolarity[channel] = &channel->registerProperty<std::string>("Polarity");

    mTxEqualization.at(channel)->setUnit("dB");
    mTxEqualization.at(channel)->setFormat(core::format::kFixedPoint, 1);

    mMetricLOL[channel] = &channel->registerMetric<bool>("LOL", "CDR lock lost (LOL)", ::swatch::core::EqualCondition<bool>(true));
  }
}


FireflyTx::~FireflyTx()
{
}


void FireflyTx::retrieveMetricValues()
{
  const auto lStatus = mController.getModuleStatus();
  setMetric(mMetricTemperature, lStatus.temperature);
}


void FireflyTx::retrievePropertyValues()
{
  set<std::string>(mPartNumber, "Firefly-123-ABC");
  set<std::string>(mFirmwareVersion, "0x1234CAFE");
}


void FireflyTx::retrieveOutputMetricValues(Channel& aChannel)
{
  const auto lStatus = mController.getTxChannelStatus(aChannel.getIndex());
  setMetric(*mMetricLOL.at(&aChannel), lStatus.lossOfLock);
}


void FireflyTx::retrieveOutputPropertyValues(Channel& aChannel)
{
  set<float>(*mTxEqualization.at(&aChannel), 5.0 + (aChannel.getIndex() / 10.0));
  set<std::string>(*mTxPolarity.at(&aChannel), (aChannel.getIndex() % 2) ? "Inverted" : "Normal");
}

} // namespace swatch
} // namespace dummy