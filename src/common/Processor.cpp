
#include "dummy/swatch/Processor.hpp"


#include "swatch/action/StateMachine.hpp"
#include "swatch/core/Factory.hpp"
#include "swatch/phase2/AlgoInterface.hpp"
#include "swatch/phase2/InputPortCollection.hpp"
#include "swatch/phase2/OutputPortCollection.hpp"

#include "dummy/Controller.hpp"
#include "dummy/swatch/Readout.hpp"
#include "dummy/swatch/RxPort.hpp"
#include "dummy/swatch/TTC.hpp"
#include "dummy/swatch/TxPort.hpp"
#include "dummy/swatch/commands/AlignLinks.hpp"
#include "dummy/swatch/commands/ConfigureOpticalRx.hpp"
#include "dummy/swatch/commands/ConfigureOpticalTx.hpp"
#include "dummy/swatch/commands/ConfigureRxBuffers.hpp"
#include "dummy/swatch/commands/ConfigureRxMGTs.hpp"
#include "dummy/swatch/commands/ConfigureTxBuffers.hpp"
#include "dummy/swatch/commands/ConfigureTxMGTs.hpp"
#include "dummy/swatch/commands/ForceComponentHealth.hpp"
#include "dummy/swatch/commands/ForceRxPortHealth.hpp"
#include "dummy/swatch/commands/ForceTxPortHealth.hpp"
#include "dummy/swatch/commands/Program.hpp"
#include "dummy/swatch/commands/Reboot.hpp"
#include "dummy/swatch/commands/Reset.hpp"
#include "dummy/swatch/commands/ResetOpticalModules.hpp"
#include "dummy/swatch/commands/SaveRxBufferData.hpp"
#include "dummy/swatch/commands/SaveTxBufferData.hpp"


SWATCH_REGISTER_CLASS(dummy::swatch::Processor)


namespace dummy {
namespace swatch {


using namespace ::swatch;

Processor::Processor(const core::AbstractStub& aStub) :
  phase2::Processor(aStub, aStub.id == "x1" ? 128 : 0),
  mController(aStub.id, kNumRxPorts, kNumTxPorts),
  mBuildInfoType((addPropertyGroup("Build info"), registerProperty<std::string>("Build type", "Build info"))),
  mBuildInfoSourceArea(registerProperty<action::Table>("Build sources", "Build info")),
  mBuildInfoTimestamp(registerProperty<std::string>("Build timestamp", "Build info"))
{
  // 1) Register commands
  auto& program = registerCommand<commands::Program>("program");
  auto& reboot = registerCommand<commands::Reboot>("reboot");
  auto& reset = registerCommand<commands::Reset>("reset");
  auto& configureRxBuffers = registerCommand<commands::ConfigureRxBuffers>("configureRxBuffers");
  auto& configureTxBuffers = registerCommand<commands::ConfigureTxBuffers>("configureTxBuffers");
  auto& saveRxData = registerCommand<commands::SaveRxBufferData>("saveRxBufferData");
  auto& saveTxData = registerCommand<commands::SaveTxBufferData>("saveTxBufferData");

  auto& configureRxMGTs = registerCommand<commands::ConfigureRxMGTs>("configureRxMGTs");
  auto& configureTxMGTs = registerCommand<commands::ConfigureTxMGTs>("configureTxMGTs");
  auto& alignLinks = registerCommand<commands::AlignLinks>("alignLinks");

  auto& resetOpticalModules = registerCommand<commands::ResetOpticalModules>("resetOpticalModules");
  auto& configureOpticalRx = registerCommand<commands::ConfigureOpticalRx>("configureOpticalRx");
  auto& configureOpticalTx = registerCommand<commands::ConfigureOpticalTx>("configureOpticalTx");

  // Dummy command, solely for testing user interfaces
  registerCommand<commands::ForceComponentHealth>("forceComponentHealth");
  registerCommand<commands::ForceRxPortHealth>("forceRxPortHealth");
  registerCommand<commands::ForceTxPortHealth>("forceTxPortHealth");

  // 2a) Populate 'algo test' state machine
  /* clang-format off */
  getPayloadTestFSM().setup
      .add(reboot)
      .add(program)
      .add(reset);
  getPayloadTestFSM().playback
      .add(configureTxBuffers)
      .add(configureRxBuffers)
      .add(saveTxData)
      .add(saveRxData);

  // 2b) Populate 'link test' state machine
  getLinkTestFSM().setup
      .add(reboot)
      .add(program)
      .add(reset);
  getLinkTestFSM().configureTx
      .add(configureTxBuffers)
      .add(configureTxMGTs)
      .add(resetOpticalModules)
      .add(configureOpticalTx);
  getLinkTestFSM().configureRx
      .add(configureOpticalRx)
      .add(configureRxMGTs)
      .add(alignLinks)
      .add(configureRxBuffers);
  getLinkTestFSM().capture
      .add(saveTxData)
      .add(saveRxData);

  // 2c) Populate 'slice test' state machine
  getSliceTestFSM().setup
      .add(reboot)
      .add(program)
      .add(reset);
  getSliceTestFSM().configure
      .add(resetOpticalModules)
      .add(configureOpticalRx)
      .add(configureRxMGTs)
      .add(alignLinks)
      .add(configureRxBuffers)
      .add(configureTxBuffers)
      .add(configureTxMGTs)
      .add(configureOpticalTx);
  getSliceTestFSM().capture
      .add(saveTxData)
      .add(saveRxData);
  /* clang-format on */

  // 3) Register monitorable components
  registerAlgo(new ::swatch::phase2::AlgoInterface());
  registerReadout(new Readout(mController));
  registerTTC(new TTC(mController));

  auto& inputCollection = registerInputs();
  for (size_t i = 0; i < kNumRxPorts; i++) {
    // Channels 0 to 15: Connected to QSFP A to D (in reverse order)
    // Channels 16 onwards: Connected to Firefly site A, B, ... (in normal order)
    // Channels 124 to 135: Connected to other FPGA (last 4 in reverse order)
    phase2::ChannelID lConnectedObject;
    if (i < 16) {
      lConnectedObject.component = std::string("qsfp") + *(getId().end() - 1) + "_";
      lConnectedObject.component += char(68 - (i / 4));
      lConnectedObject.index = 4 - (i % 4);
    }
    else if (i < (kNumRxPorts - 12)) {
      lConnectedObject.component = std::string("fireflyRx") + *(getId().end() - 1) + "_";
      lConnectedObject.component += char(65 + ((i - 16) / 12));
      lConnectedObject.index = (i - 16) % 12;
    }
    else {
      lConnectedObject.component = (aStub.id == "x0" ? "x1" : "x0");
      if (i < (kNumRxPorts - 4))
        lConnectedObject.index = i + (kNumTxPorts - kNumRxPorts);
      else
        lConnectedObject.index = kNumTxPorts - 4 + (kNumRxPorts - 1 - i);
    }

    inputCollection.addPort(new RxPort(i, mController, lConnectedObject));
  }

  auto& outputCollection = registerOutputs();
  for (size_t i = 0; i < kNumTxPorts; i++) {
    // Channels 0 to 15: Connected to QSFP A to D (in reverse order)
    // Channels 16 to 99: Connected to Firefly site A, B, ... (in normal order)
    // Channels 100 to 112: Connected to other FPGA (last 4 in reverse order)
    phase2::ChannelID lConnectedObject;
    if (i < 16) {
      lConnectedObject.component = std::string("qsfp") + *(getId().end() - 1) + "_";
      lConnectedObject.component.push_back(char(68 - (i / 4)));
      lConnectedObject.index = 4 - (i % 4);
    }
    else if (i < (kNumTxPorts - 12)) {
      lConnectedObject.component = std::string("fireflyTx") + *(getId().end() - 1) + "_";
      lConnectedObject.component.push_back(char(65 + ((i - 16) / 12)));
      lConnectedObject.index = (i - 16) % 12;
    }
    else {
      lConnectedObject.component = (aStub.id == "x0" ? "x1" : "x0");
      if (i < (kNumTxPorts - 4))
        lConnectedObject.index = i + (kNumRxPorts - kNumTxPorts);
      else
        lConnectedObject.index = kNumRxPorts - 4 + (kNumTxPorts - 1 - i);
    }

    outputCollection.addPort(new TxPort(i, mController, lConnectedObject));
  }
}


Processor::~Processor()
{
}


Controller& Processor::getController()
{
  return mController;
}


void Processor::retrievePropertyValues()
{
  action::Table lSourceInfo({ { "Repository name", typeid(std::string) }, { "Branch/Tag", typeid(std::string) }, { "SHA", typeid(std::string) }, { "Uncommitted changes?", typeid(bool) } });
  lSourceInfo.addRow({ { std::string("my-repo") }, { std::string("v0.0.0") }, { std::string("0x01010101") }, { false } });
  lSourceInfo.addRow({ { std::string("other-repo") }, { std::string("v1.2.3") }, { std::string("0x01234567") }, { true } });
  lSourceInfo.addRow({ { std::string("repo-the-third") }, { std::string("my-feature-branch") }, { std::string("0x89abcdef") }, { true } });

  set(mBuildInfoType, std::string("Automatic (project X, pipeline N, job M)"));
  set(mBuildInfoSourceArea, lSourceInfo);
  set<std::string>(mBuildInfoTimestamp, "2022-10-10 09:30:00 UTC");
}


void Processor::retrieveMetricValues()
{
}


} // namespace swatch
} // namespace dummy
