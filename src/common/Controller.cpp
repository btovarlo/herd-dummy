
#include "dummy/Controller.hpp"


#include <algorithm>
#include <stdexcept>
#include <string>

#include <boost/filesystem.hpp>

#include <yaml-cpp/node/node.h>
#include <yaml-cpp/node/parse.h>
#include <yaml-cpp/yaml.h>

#include "dummy/AlgoStatus.hpp"
#include "dummy/ReadoutStatus.hpp"
#include "dummy/RxMGTStatus.hpp"
#include "dummy/TTCStatus.hpp"
#include "dummy/TxMGTStatus.hpp"

#include "swatch/core/exception.hpp"


namespace dummy {

const std::string Controller::kDefaultLinkSourceInfoFilePath("/usr/etc/dummy-swatch/link-source-info/default.yaml");


Controller::Controller(const std::string& aId, const size_t aNumRxChannels, const size_t aNumTxChannels) :
  mId(aId),
  mClkStatus(ComponentHealth::kNotReachable),
  mReadoutStatus(ComponentHealth::kNotReachable),
  mAlgoStatus(ComponentHealth::kNotReachable),
  mLinkSourceInfo(parseLinkSourceInfoFile(getLinkSourceInfoFilepath(), mId))
{
  for (size_t i = 0; i < aNumRxChannels; i++) {
    mRxStatus[i] = ComponentHealth::kNotReachable;
    if (mLinkSourceInfo.count(i) == 0) {
      LinkSourceInfo lInfo;
      lInfo.channelId = i;
      mLinkSourceInfo[i] = lInfo;
    }
  }

  for (size_t i = 0; i < aNumTxChannels; i++)
    mTxStatus[i] = ComponentHealth::kNotReachable;
}


Controller::~Controller()
{
}


TTCStatus Controller::getTTCStatus() const
{
  // Firmware block unreachable : Control library throws
  if (mClkStatus == ComponentHealth::kNotReachable)
    throw std::runtime_error("Problem communicating with firmware (TTC block).");

  TTCStatus status;
  if (mClkStatus == ComponentHealth::kError) {
    status.bc0Lock = false;
    status.clock40Stopped = true;
  }

  return status;
}


ReadoutStatus Controller::getReadoutStatus() const
{
  // Firmware block unreachable : Control library throws
  if (mReadoutStatus == ComponentHealth::kNotReachable)
    throw std::runtime_error("Problem communicating with firmware (readout block).");

  return ReadoutStatus();
}


RxMGTStatus Controller::getRxPortStatus(uint32_t i) const
{
  const auto lIt = mRxStatus.find(i);
  if (lIt == mRxStatus.end())
    throw std::runtime_error("Rx port " + std::to_string(i) + " does not exist");

  // Firmware block unreachable : Control library throws
  if (lIt->second == ComponentHealth::kNotReachable) {
    SWATCH_THROW(swatch::core::RuntimeError("Problem communicating with firmware (rx port " + std::to_string(i) + " block)."));
  }

  RxMGTStatus status;
  if (lIt->second == ComponentHealth::kWarning)
    status.fillerErrorCount = 2;
  else if (lIt->second == ComponentHealth::kError) {
    status.crcErrorCount = 0xFFFF;
    status.headerErrorCount = 0x100;
    status.fillerErrorCount = 42;
  }

  if (lIt->second != ComponentHealth::kError) {
    status.sourceChannelId = mLinkSourceInfo.at(i).channelId;
    status.sourceSlotId = mLinkSourceInfo.at(i).slotId;
    status.sourceCrateId = mLinkSourceInfo.at(i).crateId;
  }

  return status;
}


TxMGTStatus Controller::getTxPortStatus(uint32_t i) const
{
  const auto lIt = mTxStatus.find(i);
  if (lIt == mTxStatus.end())
    throw std::runtime_error("Tx port " + std::to_string(i) + " does not exist");

  // Firmware block unreachable : Control library throws
  if (lIt->second == ComponentHealth::kNotReachable)
    throw std::runtime_error("Problem communicating with firmware (tx port " + std::to_string(i) + " block).");

  TxMGTStatus status;
  if (lIt->second == ComponentHealth::kError)
    status.qpllLocked = false;

  return status;
}


AlgoStatus Controller::getAlgoStatus() const
{
  // Firmware block unreachable : Control library throws
  if (mAlgoStatus == ComponentHealth::kNotReachable)
    throw std::runtime_error("Problem communicating with firmware (algo block).");

  return AlgoStatus();
}


void Controller::powerOff()
{
  mClkStatus = ComponentHealth::kNotReachable;
  mReadoutStatus = ComponentHealth::kNotReachable;
  mAlgoStatus = ComponentHealth::kNotReachable;

  for (auto& x : mRxStatus)
    x.second = ComponentHealth::kNotReachable;
  for (auto& x : mTxStatus)
    x.second = ComponentHealth::kNotReachable;
}


void Controller::powerOn()
{
  mClkStatus = ComponentHealth::kNotReachable;
  mReadoutStatus = ComponentHealth::kNotReachable;
  mAlgoStatus = ComponentHealth::kNotReachable;

  for (auto& x : mRxStatus)
    x.second = ComponentHealth::kNotReachable;
  for (auto& x : mTxStatus)
    x.second = ComponentHealth::kNotReachable;
}


void Controller::program()
{
  mClkStatus = ComponentHealth::kError;
  mReadoutStatus = ComponentHealth::kError;
  mAlgoStatus = ComponentHealth::kError;

  for (auto& x : mRxStatus)
    x.second = ComponentHealth::kError;
  for (auto& x : mTxStatus)
    x.second = ComponentHealth::kError;
}


void Controller::reset()
{
  mClkStatus = ComponentHealth::kGood;
  mReadoutStatus = ComponentHealth::kError;
  mAlgoStatus = ComponentHealth::kError;

  for (auto& x : mRxStatus)
    x.second = ComponentHealth::kError;
  for (auto& x : mTxStatus)
    x.second = ComponentHealth::kError;
}


void Controller::configureRxBuffers(const std::string& aDataFilePath)
{
  mRxBufferDataPath = aDataFilePath;
}


void Controller::configureTxBuffers(const std::string& aDataFilePath)
{
  mTxBufferDataPath = aDataFilePath;
}


std::string Controller::saveRxBuffers() const
{
  return mRxBufferDataPath;
}


std::string Controller::saveTxBuffers() const
{
  return mTxBufferDataPath;
}


void Controller::configureRxMGTs(const std::vector<size_t>& aIds)
{
  if (mClkStatus == ComponentHealth::kError) {
    for (auto& x : mRxStatus)
      x.second = ComponentHealth::kError;
    throw std::runtime_error("Couldn't configure rx ports - no clock!");
  }
  else {
    for (auto& x : mRxStatus)
      x.second = (std::count(aIds.begin(), aIds.end(), x.first) > 0 ? ComponentHealth::kGood : ComponentHealth::kError);
  }
}

void Controller::configureTxMGTs(const std::vector<size_t>& aIds)
{
  if (mClkStatus == ComponentHealth::kError) {
    for (auto& x : mTxStatus)
      x.second = ComponentHealth::kError;
    throw std::runtime_error("Couldn't configure tx ports - no clock!");
  }
  else {
    for (auto& x : mTxStatus)
      x.second = (std::count(aIds.begin(), aIds.end(), x.first) > 0 ? ComponentHealth::kGood : ComponentHealth::kError);
  }
}

void Controller::configureReadout()
{
  if (mClkStatus == ComponentHealth::kError) {
    mReadoutStatus = ComponentHealth::kError;
    throw std::runtime_error("Couldn't configure readout block - no clock!");
  }
  else
    mReadoutStatus = ComponentHealth::kGood;
}

void Controller::configureAlgo()
{
  if (mClkStatus == ComponentHealth::kError) {
    mAlgoStatus = ComponentHealth::kError;
    throw std::runtime_error("Couldn't configure algo - no clock!");
  }
  else
    mAlgoStatus = ComponentHealth::kGood;
}


void Controller::alignLinks()
{
}


ComponentHealth Controller::getTxHealth(size_t i) const
{
  const auto lIt = mTxStatus.find(i);
  if (lIt == mTxStatus.end())
    throw std::runtime_error("Tx port " + std::to_string(i) + " does not exist");

  return lIt->second;
}


void Controller::forceClkTtcHealth(ComponentHealth aNewStatus)
{
  mClkStatus = aNewStatus;
}


void Controller::forceRxPortHealth(size_t i, ComponentHealth aNewStatus)
{
  const auto lIt = mRxStatus.find(i);
  if (lIt == mRxStatus.end())
    throw std::runtime_error("Rx port " + std::to_string(i) + " does not exist");

  lIt->second = aNewStatus;
}


void Controller::forceTxPortHealth(size_t i, ComponentHealth aNewStatus)
{
  const auto lIt = mTxStatus.find(i);
  if (lIt == mTxStatus.end())
    throw std::runtime_error("Tx port " + std::to_string(i) + " does not exist");

  lIt->second = aNewStatus;
}


void Controller::forceReadoutHealth(ComponentHealth aNewStatus)
{
  mReadoutStatus = aNewStatus;
}


void Controller::forceAlgoHealth(ComponentHealth aNewStatus)
{
  mAlgoStatus = aNewStatus;
}


std::string Controller::getLinkSourceInfoFilepath()
{
  if (const char* lSourceInfoFilePath = std::getenv("DUMMY_SWATCH_SOURCE_INFO_FILE"))
    return lSourceInfoFilePath;
  return kDefaultLinkSourceInfoFilePath;
}


std::map<size_t, Controller::LinkSourceInfo> Controller::parseLinkSourceInfoFile(const std::string& aFilePath, const std::string& aSiteName)
{
  if (not boost::filesystem::exists(boost::filesystem::path(aFilePath)))
    throw std::runtime_error("Link source info file '" + aFilePath + "' does not exist");

  YAML::Node lRootNode = YAML::LoadFile(aFilePath);
  std::map<size_t, LinkSourceInfo> lResult;

  if (not lRootNode.IsMap())
    throw std::runtime_error("Invalid format for link source info file: Top node is not a map");

  for (YAML::const_iterator lIt = lRootNode.begin(); lIt != lRootNode.end(); lIt++) {
    const std::string lSiteName(lIt->first.as<std::string>());
    if (lSiteName != aSiteName)
      continue;

    if (not lIt->second.IsMap())
      throw std::runtime_error("Invalid format for link source info file: Node '" + lSiteName + "' is not a map");

    for (YAML::const_iterator lIt2 = lIt->second.begin(); lIt2 != lIt->second.end(); lIt2++) {
      const std::string lKey(lIt2->first.as<std::string>());
      if (lKey.find_first_not_of("0123456789") != std::string::npos)
        throw std::runtime_error("Invalid format for link source info file: Key '" + lKey + "' in '" + lSiteName + "' has invalid characters (should be a channel index)");

      const size_t lRxChannel = std::stoul(lKey);

      if (not lIt2->second.IsSequence())
        throw std::runtime_error("Invalid format for link source info file: Entry for device '" + lSiteName + "' channel " + lKey + " is not an array");

      if (lIt2->second.size() != 3)
        throw std::runtime_error("Invalid format for link source info file: Entry for device '" + lSiteName + "' channel " + lKey + " has incorrect length (should be 3)");

      if (not lIt2->second[0].IsScalar() or lIt2->second[0].as<size_t>() >= 256)
        throw std::runtime_error("Invalid format for link source info file: 1st item (source crate ID) in array for device '" + lSiteName + "' channel " + lKey + " should be a positive integer, less than 256");

      if (not lIt2->second[1].IsScalar() or lIt2->second[1].as<size_t>() >= 16)
        throw std::runtime_error("Invalid format for link source info file: 2nd item (source slot ID) in array for device '" + lSiteName + "' channel " + lKey + " should be a positive integer, less than 16");

      if (not lIt2->second[2].IsScalar() or lIt2->second[2].as<size_t>() >= 256)
        throw std::runtime_error("Invalid format for link source info file: 3rd item (source channel ID) in array for device '" + lSiteName + "' channel " + lKey + " should be a positive integer, less than 256");

      LinkSourceInfo lSourceInfo;
      lSourceInfo.crateId = lIt2->second[0].as<size_t>();
      lSourceInfo.slotId = lIt2->second[1].as<size_t>();
      lSourceInfo.channelId = lIt2->second[2].as<size_t>();

      lResult[lRxChannel] = lSourceInfo;
    }
  }
  return lResult;
}

} // namespace dummy
