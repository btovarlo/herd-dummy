
#include "swatch/phase2/Board.hpp"
#include "swatch/phase2/EmptySocket.hpp"

#include "dummy/OpticsController.hpp"
#include "dummy/swatch/FireflyRx.hpp"
#include "dummy/swatch/FireflyTx.hpp"
#include "dummy/swatch/QSFP.hpp"


#include <iostream>

namespace dummy {
namespace swatch {

std::pair<std::vector<::swatch::phase2::OpticalModule*>, std::vector<std::string>> createOpticalModules()
{
  std::vector<::swatch::phase2::OpticalModule*> lModules;

  // 4 QSFP sites per FPGA (A to D), each occupied
  for (const auto lSuffix : std::vector<std::string> { "A", "B", "C", "D" }) {
    lModules.push_back(new QSFP("qsfp0_" + lSuffix));
    lModules.push_back(new QSFP("qsfp1_" + lSuffix));
  }

  // 9 Firefly RX & 9 Firefly TX sites. A to G occupied, H & I empty
  for (const auto lSuffix : std::vector<std::string> { "A", "B", "C", "D", "E", "F", "G" }) {
    lModules.push_back(new FireflyRx("fireflyRx0_" + lSuffix));
    lModules.push_back(new FireflyRx("fireflyRx1_" + lSuffix));
    lModules.push_back(new FireflyTx("fireflyTx0_" + lSuffix));
    lModules.push_back(new FireflyTx("fireflyTx1_" + lSuffix));
  }

  using ::swatch::phase2::EmptySocket;
  for (const auto lSuffix : std::vector<std::string> { "H", "I" }) {
    lModules.push_back(new EmptySocket("fireflyRx0_" + lSuffix, 12 /* N_inputs */, 0 /* N_outputs */));
    lModules.push_back(new EmptySocket("fireflyRx1_" + lSuffix, 12 /* N_inputs */, 0 /* N_outputs */));
    lModules.push_back(new EmptySocket("fireflyTx0_" + lSuffix, 0 /* N_inputs */, 12 /* N_outputs */));
    lModules.push_back(new EmptySocket("fireflyTx1_" + lSuffix, 0 /* N_inputs */, 12 /* N_outputs */));

    OpticsController::create("fireflyRx0_" + lSuffix, { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 }, {});
    OpticsController::create("fireflyRx1_" + lSuffix, { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 }, {});
    OpticsController::create("fireflyTx0_" + lSuffix, {}, { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 });
    OpticsController::create("fireflyTx1_" + lSuffix, {}, { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 });
  }

  return { lModules, { "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19" } };
}

} // namespace swatch
} // namespace exampleboard

SWATCH_REGISTER_PHASE2_OPTICS_CREATOR("dummy", dummy::swatch::createOpticalModules);
