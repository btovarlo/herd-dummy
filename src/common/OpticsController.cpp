
#include "dummy/OpticsController.hpp"


#include <algorithm>
#include <cstdlib>
#include <stdexcept>
#include <string>

#include "dummy/OpticalModuleStatus.hpp"
#include "dummy/OpticalRxChannelStatus.hpp"
#include "dummy/OpticalTxChannelStatus.hpp"


namespace dummy {

std::unordered_map<std::string, std::unique_ptr<OpticsController>> OpticsController::sControllerMap {};


OpticsController::OpticsController(const std::vector<size_t>& aRxChannelIndices, const std::vector<size_t>& aTxChannelIndices) :
  mModuleStatus(ComponentHealth::kNotReachable)
{
  for (const auto i : aRxChannelIndices)
    mRxChannelStatus[i] = ComponentHealth::kNotReachable;
  for (const auto i : aTxChannelIndices)
    mTxChannelStatus[i] = ComponentHealth::kNotReachable;
}


OpticsController::~OpticsController()
{
}


ComponentHealth OpticsController::getModuleHealth() const
{
  return mModuleStatus;
}


ComponentHealth OpticsController::getRxChannelHealth(size_t i) const
{
  const auto lIt = mRxChannelStatus.find(i);
  if (lIt == mRxChannelStatus.end())
    throw std::runtime_error("Optical module rx channel " + std::to_string(i) + " does not exist");
  return lIt->second;
}


ComponentHealth OpticsController::getTxChannelHealth(size_t i) const
{
  const auto lIt = mTxChannelStatus.find(i);
  if (lIt == mTxChannelStatus.end())
    throw std::runtime_error("Optical module tx channel " + std::to_string(i) + " does not exist");
  return lIt->second;
}


OpticalModuleStatus OpticsController::getModuleStatus() const
{
  // Firmware block unreachable : Control library throws
  if (mModuleStatus == ComponentHealth::kNotReachable)
    throw std::runtime_error("Problem communicating with optical module.");

  OpticalModuleStatus status;
  status.temperature = 30.0 + 10.0 * float(std::rand()) / RAND_MAX;
  if (mModuleStatus == ComponentHealth::kError) {
    status.temperature += 20.0;
  }

  return status;
}


OpticalRxChannelStatus OpticsController::getRxChannelStatus(size_t i) const
{
  const auto lIt = mRxChannelStatus.find(i);
  if (lIt == mRxChannelStatus.end())
    throw std::runtime_error("Optical module rx channel " + std::to_string(i) + " does not exist");

  if (lIt->second == ComponentHealth::kNotReachable)
    throw std::runtime_error("Problem communicating with optical module (rx channel " + std::to_string(i) + ")");

  OpticalRxChannelStatus lStatus;
  if (lIt->second == ComponentHealth::kError) {
    lStatus.power = 0.01;
    lStatus.lossOfLock = true;
    lStatus.lossOfSignal = true;
  }
  else if (lIt->second == ComponentHealth::kWarning)
    lStatus.power = 0.8;
  else
    lStatus.power = 1.0 + 0.1 * float(std::rand()) / RAND_MAX;

  return lStatus;
}


OpticalTxChannelStatus OpticsController::getTxChannelStatus(size_t i) const
{
  const auto lIt = mTxChannelStatus.find(i);
  if (lIt == mTxChannelStatus.end())
    throw std::runtime_error("Optical module tx channel " + std::to_string(i) + " does not exist");

  if (lIt->second == ComponentHealth::kNotReachable)
    throw std::runtime_error("Problem communicating with optical module (tx channel " + std::to_string(i) + ")");

  OpticalTxChannelStatus lStatus;
  if (lIt->second == ComponentHealth::kError) {
    lStatus.lossOfLock = true;
  }

  return lStatus;
}


void OpticsController::powerOff()
{
  mModuleStatus = ComponentHealth::kNotReachable;

  for (auto& x : mRxChannelStatus)
    x.second = ComponentHealth::kNotReachable;
  for (auto& x : mTxChannelStatus)
    x.second = ComponentHealth::kNotReachable;
}


void OpticsController::powerOn()
{
  mModuleStatus = ComponentHealth::kGood;

  for (auto& x : mRxChannelStatus)
    x.second = ComponentHealth::kError;
  for (auto& x : mTxChannelStatus)
    x.second = ComponentHealth::kError;
}


void OpticsController::reset()
{
  mModuleStatus = ComponentHealth::kGood;

  for (auto& x : mRxChannelStatus)
    x.second = ComponentHealth::kError;
  for (auto& x : mTxChannelStatus)
    x.second = ComponentHealth::kError;
}


void OpticsController::configureRx(const size_t aIndex)
{
  auto lIt = mRxChannelStatus.find(aIndex);
  if (lIt == mRxChannelStatus.end())
    throw std::runtime_error("Optical module rx channel " + std::to_string(aIndex) + " does not exist");

  if (mModuleStatus == ComponentHealth::kError) {
    lIt->second = ComponentHealth::kError;
    throw std::runtime_error("Couldn't configure rx channel " + std::to_string(aIndex) + " - module in error!");
  }
  else
    lIt->second = ComponentHealth::kGood;
}


void OpticsController::configureTx(const size_t aIndex)
{
  auto lIt = mTxChannelStatus.find(aIndex);
  if (lIt == mTxChannelStatus.end())
    throw std::runtime_error("Optical module tx channel " + std::to_string(aIndex) + " does not exist");

  if (mModuleStatus == ComponentHealth::kError) {
    lIt->second = ComponentHealth::kError;
    throw std::runtime_error("Couldn't configure tx channel " + std::to_string(aIndex) + " - module in error!");
  }
  else
    lIt->second = ComponentHealth::kGood;
}


void OpticsController::forceModuleHealth(ComponentHealth aNewStatus)
{
  mModuleStatus = aNewStatus;
}


void OpticsController::forceRxChannelHealth(size_t i, ComponentHealth aNewStatus)
{
  const auto lIt = mRxChannelStatus.find(i);
  if (lIt == mRxChannelStatus.end())
    throw std::runtime_error("Optical module rx channel " + std::to_string(i) + " does not exist");

  lIt->second = aNewStatus;
}


void OpticsController::forceTxChannelHealth(size_t i, ComponentHealth aNewStatus)
{
  const auto lIt = mTxChannelStatus.find(i);
  if (lIt == mTxChannelStatus.end())
    throw std::runtime_error("Optical module tx channel " + std::to_string(i) + " does not exist");

  lIt->second = aNewStatus;
}


OpticsController* OpticsController::get(const std::string& aId)
{
  auto lIt = sControllerMap.find(aId);
  if (lIt == sControllerMap.end())
    return NULL;
  else
    return lIt->second.get();
}


OpticsController& OpticsController::create(const std::string& aId, const std::vector<size_t>& aRx, const std::vector<size_t>& aTx)
{
  auto lIt = sControllerMap.find(aId);
  if (lIt != sControllerMap.end())
    throw std::runtime_error("Optics controller \"" + aId + "\" already exists");

  sControllerMap[aId] = std::unique_ptr<OpticsController>(new OpticsController(aRx, aTx));
  return *sControllerMap.at(aId);
}


std::unordered_map<std::string, std::unique_ptr<OpticsController>>::const_iterator OpticsController::begin()
{
  return sControllerMap.cbegin();
}


std::unordered_map<std::string, std::unique_ptr<OpticsController>>::const_iterator OpticsController::end()
{
  return sControllerMap.cend();
}

} // namespace dummy
