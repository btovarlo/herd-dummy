#include "dummy/swatch/QSFP.hpp"

#include "swatch/action/Property.hpp"
#include "swatch/action/PropertyHolder.hxx"
#include "swatch/core/MetricConditions.hpp"

#include "dummy/OpticalModuleStatus.hpp"
#include "dummy/OpticalRxChannelStatus.hpp"
#include "dummy/OpticalTxChannelStatus.hpp"
#include "dummy/OpticsController.hpp"


namespace dummy {
namespace swatch {

using namespace ::swatch;

QSFP::QSFP(const std::string& aId) :
  OpticalModule(aId, { 1, 2, 3, 4 }, { 1, 2, 3, 4 }),
  mController(OpticsController::create(aId, { 1, 2, 3, 4 }, { 1, 2, 3, 4 })),
  mPartNumber(registerProperty<std::string>("Part number")),
  mFirmwareVersion(registerProperty<std::string>("Firmware version")),
  mMetricTemperature(registerMetric<float>("temperature", ::swatch::core::GreaterThanCondition<float>(50)))
{
  mMetricTemperature.setUnit("C");
  mMetricTemperature.setFormat(core::format::kFixedPoint, 1);

  setAsPresent();

  for (auto* channel : getInputs()) {
    mRxAmplitude[channel] = &channel->registerProperty<float>("Amplitude");
    mRxPolarity[channel] = &channel->registerProperty<std::string>("Polarity");

    mMetricRxPower[channel] = &channel->registerMetric<float>("power", "Power (mW)");
    mMetricRxLOL[channel] = &channel->registerMetric<bool>("LOL", "CDR lock lost (LOL)", ::swatch::core::EqualCondition<bool>(true));
    mMetricRxLOS[channel] = &channel->registerMetric<bool>("LOS", "Signal lost (LOS)", ::swatch::core::EqualCondition<bool>(true));

    mRxAmplitude.at(channel)->setUnit("mV");
    mRxAmplitude.at(channel)->setFormat(core::format::kFixedPoint, 1);
    mMetricRxPower.at(channel)->setUnit("mW");
    mMetricRxPower.at(channel)->setFormat(core::format::kFixedPoint, 4);
  }

  for (auto* channel : getOutputs()) {
    mTxEqualization[channel] = &channel->registerProperty<float>("Equalization");
    mTxPolarity[channel] = &channel->registerProperty<std::string>("Polarity");

    mTxEqualization.at(channel)->setUnit("dB");
    mTxEqualization.at(channel)->setFormat(core::format::kFixedPoint, 1);

    mMetricTxLOL[channel] = &channel->registerMetric<bool>("LOL", "CDR lock lost (LOL)", ::swatch::core::EqualCondition<bool>(true));
  }
}


QSFP::~QSFP()
{
}


void QSFP::retrieveMetricValues()
{
  const auto lStatus = mController.getModuleStatus();
  setMetric(mMetricTemperature, lStatus.temperature);
}


void QSFP::retrievePropertyValues()
{
  set<std::string>(mPartNumber, "QSFP-1234-ABCD");
  set<std::string>(mFirmwareVersion, "0x0011223344");
}


void QSFP::retrieveInputMetricValues(Channel& aChannel)
{
  const auto lStatus = mController.getRxChannelStatus(aChannel.getIndex());
  setMetric(*mMetricRxPower.at(&aChannel), lStatus.power);
  setMetric(*mMetricRxLOL.at(&aChannel), lStatus.lossOfLock);
  setMetric(*mMetricRxLOS.at(&aChannel), lStatus.lossOfSignal);
}


void QSFP::retrieveInputPropertyValues(Channel& aChannel)
{
  set<float>(*mRxAmplitude.at(&aChannel), 1.0 + (aChannel.getIndex() / 10.0));
  set<std::string>(*mRxPolarity.at(&aChannel), (aChannel.getIndex() % 2) ? "Normal" : "Inverted");
}


void QSFP::retrieveOutputMetricValues(Channel& aChannel)
{
  const auto lStatus = mController.getTxChannelStatus(aChannel.getIndex());
  setMetric(*mMetricTxLOL.at(&aChannel), lStatus.lossOfLock);
}


void QSFP::retrieveOutputPropertyValues(Channel& aChannel)
{
  set<float>(*mTxEqualization.at(&aChannel), 5.0 + (aChannel.getIndex() / 10.0));
  set<std::string>(*mTxPolarity.at(&aChannel), (aChannel.getIndex() % 2) ? "Inverted" : "Normal");
}

} // namespace swatch
} // namespace dummy