#include "dummy/swatch/FireflyRx.hpp"

#include "swatch/core/MetricConditions.hpp"

#include "dummy/OpticalModuleStatus.hpp"
#include "dummy/OpticalRxChannelStatus.hpp"
#include "dummy/OpticsController.hpp"


namespace dummy {
namespace swatch {

using namespace ::swatch;

FireflyRx::FireflyRx(const std::string& aId) :
  OpticalModule(aId, { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 }, {}),
  mController(OpticsController::create(aId, { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 }, {})),
  mPartNumber(registerProperty<std::string>("Part number")),
  mFirmwareVersion(registerProperty<std::string>("Firmware version")),
  mMetricTemperature(registerMetric<float>("temperature", ::swatch::core::GreaterThanCondition<float>(50)))
{
  mMetricTemperature.setUnit("C");
  mMetricTemperature.setFormat(core::format::kFixedPoint, 1);

  setAsPresent();

  for (auto* channel : getInputs()) {
    mRxAmplitude[channel] = &channel->registerProperty<float>("Amplitude");
    mRxPolarity[channel] = &channel->registerProperty<std::string>("Polarity");

    mMetricPower[channel] = &channel->registerMetric<float>("power", "Power");
    mMetricLOL[channel] = &channel->registerMetric<bool>("LOL", "CDR lock lost (LOL)", ::swatch::core::EqualCondition<bool>(true));
    mMetricLOS[channel] = &channel->registerMetric<bool>("LOS", "Signal lost (LOS)", ::swatch::core::EqualCondition<bool>(true));

    mRxAmplitude.at(channel)->setUnit("mV");
    mRxAmplitude.at(channel)->setFormat(core::format::kFixedPoint, 1);
    mMetricPower.at(channel)->setUnit("mW");
    mMetricPower.at(channel)->setFormat(core::format::kFixedPoint, 4);
  }
}


FireflyRx::~FireflyRx()
{
}


void FireflyRx::retrieveMetricValues()
{
  const auto lStatus = mController.getModuleStatus();
  setMetric(mMetricTemperature, lStatus.temperature);
}


void FireflyRx::retrievePropertyValues()
{
  set<std::string>(mPartNumber, "Firefly-123-XYZ");
  set<std::string>(mFirmwareVersion, "0xDEADBEEF");
}


void FireflyRx::retrieveInputMetricValues(Channel& aChannel)
{
  const auto lStatus = mController.getRxChannelStatus(aChannel.getIndex());
  setMetric(*mMetricPower.at(&aChannel), lStatus.power);
  setMetric(*mMetricLOL.at(&aChannel), lStatus.lossOfLock);
  setMetric(*mMetricLOS.at(&aChannel), lStatus.lossOfSignal);
}


void FireflyRx::retrieveInputPropertyValues(Channel& aChannel)
{
  set<float>(*mRxAmplitude.at(&aChannel), 1.0 + (aChannel.getIndex() / 10.0));
  set<std::string>(*mRxPolarity.at(&aChannel), (aChannel.getIndex() % 2) ? "Normal" : "Inverted");
}


} // namespace swatch
} // namespace dummy