
#include "dummy/swatch/TTC.hpp"


#include "dummy/Controller.hpp"
#include "dummy/TTCStatus.hpp"


namespace dummy {
namespace swatch {

using namespace ::swatch;

TTC::TTC(const Controller& aController) :
  TTCInterface(),
  mController(aController)
{
}


TTC::~TTC()
{
}


void TTC::retrieveMetricValues()
{
  const auto status = mController.getTTCStatus();

  setMetric(mMetricIsClock40Locked, status.clock40Lock);
  setMetric(mMetricHasClock40Stopped, status.clock40Stopped);
  setMetric(mMetricIsBC0Locked, status.bc0Lock);

  setMetric(mMetricL1ACounter, status.l1ACounter);
  setMetric(mMetricBunchCounter, status.bunchCounter);
  setMetric(mMetricOrbitCounter, status.orbitCounter);
}

} // namespace swatch
} // namespace dummy
