
#include "dummy/swatch/commands/ConfigureTxMGTs.hpp"


#include "swatch/core/rules/NonEmptyString.hpp"
#include "swatch/phase2/OutputPort.hpp"
#include "swatch/phase2/OutputPortCollection.hpp"

#include "dummy/swatch/Processor.hpp"
#include "dummy/swatch/commands/utilities.hpp"
#include "dummy/swatch/utilities.hpp"


namespace dummy {
namespace swatch {
namespace commands {

using namespace ::swatch;
using namespace std::string_literals;


ConfigureTxMGTs::ConfigureTxMGTs(const std::string& aId, action::ActionableObject& aActionable) :
  Command(aId, aActionable, "Dummy command's default result!"s)
{
  registerDummyParameters(*this);
  registerParameter<std::string>({ "ids"s, "List of channel indices" }, "0-79", core::rules::NonEmptyString<std::string>());
}

ConfigureTxMGTs::~ConfigureTxMGTs()
{
}

action::Command::State ConfigureTxMGTs::code(const core::ParameterSet& aParams)
{
  const std::vector<size_t> lIds(utilities::parseListOfIndices(aParams.get<std::string>("ids")));

  // Filter out masked & absent ports
  std::vector<size_t> lPortsToConfigure, lMaskedPorts, lAbsentPorts;
  for (const size_t i : lIds) {
    const auto& lPort = getActionable<Processor>().getOutputPorts().getPort(i);
    if (not lPort.isPresent())
      lAbsentPorts.push_back(i);
    else if (lPort.isMasked())
      lMaskedPorts.push_back(i);
    else
      lPortsToConfigure.push_back(i);
  }

  std::ostringstream lMessageStream;
  lMessageStream << "Configuring Tx MGTs " << core::shortVecFmt(lPortsToConfigure);
  if ((not lAbsentPorts.empty()) or (not lMaskedPorts.empty())) {
    lMessageStream << "(";
    if (not lAbsentPorts.empty())
      lMessageStream << "channels " << core::shortVecFmt(lAbsentPorts) << " absent";
    if ((not lAbsentPorts.empty()) and (not lMaskedPorts.empty()))
      lMessageStream << "; ";
    if (not lMaskedPorts.empty())
      lMessageStream << "channels " << core::shortVecFmt(lMaskedPorts) << " masked";
    lMessageStream << ")";
  }
  setProgress(0.0, lMessageStream.str());

  // Declare that selected output ports will be configured by the end of this command
  enableMonitoring(phase2::processorIds::kOutputPorts);
  for (auto& port : getActionable<Processor>().getOutputPorts().getPorts()) {
    if ((std::count(lPortsToConfigure.begin(), lPortsToConfigure.end(), port->getIndex()) > 0) and port->isPresent())
      enableMonitoring(phase2::processorIds::kOutputPorts + "." + port->getId());
    else
      disableMonitoring(phase2::processorIds::kOutputPorts + "." + port->getId());
  }

  // [For UI tests] Parse dummy parameters and register some dummy messages
  const auto s = readDummyParametersAndPrintMessages(aParams, [&](float x, const std::string& m) { this->setProgress(x, m); });

  // Run the configure tx MGTs method of SWATCH-independent controller
  Controller& lController = getActionable<Processor>().getController();
  lController.configureTxMGTs(lPortsToConfigure);

  // [For UI tests] If command will fail, then for consistency, tell controller
  //                to return bad monitoring data values in future for RX ports
  if (s == action::Command::kError) {
    for (const auto& port : getActionable<Processor>().getOutputPorts().getPorts())
      lController.forceTxPortHealth(port->getIndex(), ComponentHealth::kError);
  }

  return s;
}


} // namespace commands
} // namespace swatch
} // namespace dummy
