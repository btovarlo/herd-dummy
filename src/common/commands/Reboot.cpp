
#include "dummy/swatch/commands/Reboot.hpp"


#include "dummy/OpticsController.hpp"
#include "dummy/swatch/Processor.hpp"
#include "dummy/swatch/commands/utilities.hpp"


namespace dummy {
namespace swatch {
namespace commands {

using namespace ::swatch;
using namespace std::string_literals;


Reboot::Reboot(const std::string& aId, action::ActionableObject& aActionable) :
  Command(aId, aActionable, "Dummy command's default result!"s)
{
  registerDummyParameters(*this);
}

Reboot::~Reboot()
{
}

action::Command::State Reboot::code(const core::ParameterSet& aParams)
{
  // [For UI tests] Parse dummy parameters and register some dummy messages
  const auto s = readDummyParametersAndPrintMessages(aParams, [&](float x, const std::string& m) { this->setProgress(x, m); });

  // Run the power on & off methods of SWATCH-independent controller
  Controller& lController = getActionable<Processor>().getController();
  lController.powerOff();
  lController.powerOn();

  for (auto lIt = OpticsController::begin(); lIt != OpticsController::end(); lIt++) {
    lIt->second->powerOff();
    lIt->second->powerOn();
  }

  // TODO: Update framework to change monitorable settings in more declarative fashion
  //       E.g. additoinal member function, or return struct with map/list of monitoring::Status -> components
  using namespace ::swatch::phase2::processorIds;
  disableMonitoring({ kReadout, kInputPorts, kOutputPorts, kTTC });
  // ===============================================================================

  return s;
}


} // namespace commands
} // namespace swatch
} // namespace dummy
