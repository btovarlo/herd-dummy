
#include "dummy/swatch/commands/Program.hpp"


#include "swatch/action/File.hpp"

#include "dummy/swatch/Processor.hpp"
#include "dummy/swatch/commands/utilities.hpp"


namespace dummy {
namespace swatch {
namespace commands {

using namespace ::swatch;
using namespace std::string_literals;


Program::Program(const std::string& aId, action::ActionableObject& aActionable) :
  Command(aId, aActionable, "Dummy command's default result!"s)
{
  registerDummyParameters(*this);
  registerParameter<action::File>("package", { "/path/to/package.tgz", "", "" });
}

Program::~Program()
{
}

action::Command::State Program::code(const core::ParameterSet& aParams)
{
  // [For UI tests] Parse dummy parameters and register some dummy messages
  const auto s = readDummyParametersAndPrintMessages(aParams, [&](float x, const std::string& m) { this->setProgress(x, m); });

  // Run the program method of SWATCH-independent controller
  Controller& lController = getActionable<Processor>().getController();
  lController.program();

  // TODO: Update framework to change monitorable settings in more declarative fashion
  //       E.g. additoinal member function, or return struct with map/list of monitoring::Status -> components
  using namespace ::swatch::phase2::processorIds;
  disableMonitoring({ kReadout, kInputPorts, kOutputPorts, kTTC });
  // ===============================================================================

  return s;
}


} // namespace commands
} // namespace swatch
} // namespace dummy
