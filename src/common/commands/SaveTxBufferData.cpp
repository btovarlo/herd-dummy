
#include "dummy/swatch/commands/SaveTxBufferData.hpp"


#include <boost/filesystem.hpp>

#include "swatch/action/File.hpp"

#include "dummy/swatch/Processor.hpp"
#include "dummy/swatch/commands/utilities.hpp"


namespace dummy {
namespace swatch {
namespace commands {

using namespace ::swatch;
using namespace std::string_literals;


SaveTxBufferData::SaveTxBufferData(const std::string& aId, action::ActionableObject& aActionable) :
  Command(aId, "", "Capture data from TX buffers and write to file", aActionable, "Dummy command's default result!"s)
{
  registerDummyParameters(*this);
}


SaveTxBufferData::~SaveTxBufferData()
{
}


action::Command::State SaveTxBufferData::code(const core::ParameterSet& aParams)
{
  // [For UI tests] Parse dummy parameters and register some dummy messages
  const auto s = readDummyParametersAndPrintMessages(aParams, [&](float x, const std::string& m) { this->setProgress(x, m); });

  // Return TX buffer data file from controller
  const std::string lFilePath = getActionable<Processor>().getController().saveTxBuffers();
  setResult(boost::any(action::File(lFilePath, ::swatch::phase2::fileTypes::kTxBufferData, "")));

  return s;
}


} // namespace commands
} // namespace swatch
} // namespace dummy
