
#include "dummy/swatch/commands/AlignLinks.hpp"


#include "swatch/core/rules/All.hpp"
#include "swatch/core/rules/NonEmptyString.hpp"

#include "dummy/swatch/Processor.hpp"
#include "dummy/swatch/commands/SameSizeConstraint.hpp"
#include "dummy/swatch/commands/utilities.hpp"
#include "dummy/swatch/utilities.hpp"


namespace dummy {
namespace swatch {
namespace commands {


using namespace ::swatch;
using namespace std::string_literals;



AlignLinks::AlignLinks(const std::string& aId, action::ActionableObject& aActionable) :
  Command(aId, "", "Align data from input links", aActionable, "Dummy command's default result!"s)
{
  registerDummyParameters(*this);

  // TODO: Replace ID channel spec with IOChannelSelector-type mechanism?
  //       (e.g. treating parameter as mask, adding ability to invert, auto-select subset with appropriate FW instantiated)
  registerParameter<std::vector<std::string>>({ "ids"s, "List of channel indices" }, std::vector<std::string>(), core::rules::All<std::string>(core::rules::NonEmptyString<std::string>()));

  registerParameter<std::vector<uint32_t>>("target", std::vector<uint32_t>());

  // Declare constraints
  addConstraint("sameSize", SameSizeConstraint({ "ids" }, { "target" }, {}));
}

AlignLinks::~AlignLinks()
{
}

action::Command::State AlignLinks::code(const core::ParameterSet& aParams)
{
  // [For UI tests] Parse dummy parameters and register some dummy messages
  const auto s = readDummyParametersAndPrintMessages(aParams, [&](float x, const std::string& m) { this->setProgress(x, m); });

  for (const auto& lIdsString : aParams.get<std::vector<std::string>>("ids")) {
    // Run the align links method of SWATCH-independent controller
    const std::vector<size_t> lIds(utilities::parseListOfIndices(lIdsString));
    Controller& lController = getActionable<Processor>().getController();
    lController.alignLinks();

    // [For UI tests] If command will fail, then for consistency, tell controller
    //                to return bad monitoring data values in future for RX ports
    if (s == action::Command::kError) {
      for (auto i : lIds)
        lController.forceRxPortHealth(i, ComponentHealth::kError);
    }
  }

  return s;
}


} // namespace commands
} // namespace swatch
} // namespace dummy
