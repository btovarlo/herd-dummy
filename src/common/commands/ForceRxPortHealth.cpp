
#include "dummy/swatch/commands/ForceRxPortHealth.hpp"


#include "swatch/core/rules/IsAmong.hpp"
#include "swatch/core/rules/NonEmptyString.hpp"

#include "dummy/swatch/Processor.hpp"
#include "dummy/swatch/commands/utilities.hpp"
#include "dummy/swatch/utilities.hpp"


namespace dummy {
namespace swatch {
namespace commands {

using namespace ::swatch;
using namespace std::string_literals;


const std::map<std::string, ComponentHealth> ForceRxPortHealth::kComponentHealthMap {
  { "Good", ComponentHealth::kGood },
  { "Warning", ComponentHealth::kWarning },
  { "Error", ComponentHealth::kError },
  { "Not reachable", ComponentHealth::kNotReachable }
};


ForceRxPortHealth::ForceRxPortHealth(const std::string& aId, action::ActionableObject& aActionable) :
  Command(aId, aActionable, ""s)
{
  registerParameter<std::string>({ "ids"s, "List of channel indices" }, "0-99", core::rules::NonEmptyString<std::string>());

  std::vector<std::string> names;
  for (const auto x : kComponentHealthMap)
    names.push_back(x.first);
  registerParameter("status", *names.begin(), core::rules::IsAmong<std::string>(names));
}


ForceRxPortHealth::~ForceRxPortHealth()
{
}


action::Command::State ForceRxPortHealth::code(const core::ParameterSet& aParams)
{
  const std::vector<size_t> lIds(utilities::parseListOfIndices(aParams.get<std::string>("ids")));
  const auto status = kComponentHealthMap.at(aParams.get<std::string>("status"));

  for (const auto i : lIds)
    getActionable<Processor>().getController().forceRxPortHealth(i, status);

  return action::Command::kDone;
}



} // namespace commands
} // namespace swatch
} // namespace dummy
