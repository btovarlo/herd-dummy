
#include "dummy/swatch/commands/utilities.hpp"


#include <chrono>
#include <thread>

#include "swatch/action/Command.hpp"


namespace dummy {
namespace swatch {
namespace commands {

using namespace ::swatch;
using namespace std::string_literals;

void registerDummyParameters(action::Command& aCommand)
{
  aCommand.registerParameter("cmdDuration", uint32_t(5));
  aCommand.registerParameter<bool>({ "returnWarning"s, "Force dummy command to return warning" }, false);
  aCommand.registerParameter<bool>({ "returnError"s, "Force dummy command to return error" }, false);
  aCommand.registerParameter<bool>({ "throw"s, "Force dummy command to throw" }, false);
}


action::Command::State readDummyParametersAndPrintMessages(const core::ParameterSet& aParams, const std::function<void(float, const std::string&)> aSetProgress)
{
  if (aParams.get<bool>("throw"))
    throw core::RuntimeError("An exceptional error occurred!");

  action::Command::State lState = action::Command::kDone;
  if (aParams.get<bool>("returnError"))
    lState = action::Command::kError;
  else if (aParams.get<bool>("returnWarning"))
    lState = action::Command::kWarning;

  const size_t lNrSeconds = aParams.get<uint32_t>("cmdDuration");
  for (size_t i = 0; i < (lNrSeconds * 2); i++) {
    std::this_thread::sleep_for(std::chrono::milliseconds(500));
    std::ostringstream lMsg;
    lMsg << "Done " << i + 1 << " of " << (lNrSeconds * 2) << " things";
    aSetProgress(float(i) / float(lNrSeconds * 2), lMsg.str());
  }

  return lState;
}

} // namespace commands
} // namespace swatch
} // namespace dummy
