
#include "dummy/swatch/commands/ForceComponentHealth.hpp"


#include "swatch/core/rules/IsAmong.hpp"

#include "dummy/swatch/Processor.hpp"
#include "dummy/swatch/commands/utilities.hpp"


namespace dummy {
namespace swatch {
namespace commands {

using namespace ::swatch;
using namespace std::string_literals;


const std::map<std::string, std::function<void(Controller*, ComponentHealth)>> ForceComponentHealth::kMemberFunctionMap {
  { "Algo", &Controller::forceAlgoHealth },
  { "TTC", &Controller::forceClkTtcHealth },
  { "Readout", &Controller::forceReadoutHealth }
};


const std::map<std::string, ComponentHealth> ForceComponentHealth::kComponentHealthMap {
  { "Good", ComponentHealth::kGood },
  { "Warning", ComponentHealth::kWarning },
  { "Error", ComponentHealth::kError },
  { "Not reachable", ComponentHealth::kNotReachable }
};


ForceComponentHealth::ForceComponentHealth(const std::string& aId, action::ActionableObject& aActionable) :
  Command(aId, aActionable, ""s)
{
  std::vector<std::string> names;
  for (const auto x : kMemberFunctionMap)
    names.push_back(x.first);
  registerParameter("component", *names.begin(), core::rules::IsAmong<std::string>(names));

  names.clear();
  for (const auto x : kComponentHealthMap)
    names.push_back(x.first);
  registerParameter("status", *names.begin(), core::rules::IsAmong<std::string>(names));
}

ForceComponentHealth::~ForceComponentHealth()
{
}

action::Command::State ForceComponentHealth::code(const core::ParameterSet& aParams)
{
  const auto f = kMemberFunctionMap.at(aParams.get<std::string>("component"));
  const auto status = kComponentHealthMap.at(aParams.get<std::string>("status"));

  Controller& c = getActionable<Processor>().getController();
  f(&c, status);

  return action::Command::kDone;
}



} // namespace commands
} // namespace swatch
} // namespace dummy
