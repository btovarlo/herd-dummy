
#include "dummy/swatch/commands/ConfigureOpticalTx.hpp"


#include "swatch/core/rules/NonEmptyString.hpp"
#include "swatch/phase2/OutputPort.hpp"
#include "swatch/phase2/OutputPortCollection.hpp"

#include "dummy/Controller.hpp"
#include "dummy/OpticsController.hpp"
#include "dummy/swatch/Processor.hpp"
#include "dummy/swatch/TxPort.hpp"
#include "dummy/swatch/commands/utilities.hpp"
#include "dummy/swatch/utilities.hpp"


namespace dummy {
namespace swatch {
namespace commands {

using namespace ::swatch;
using namespace std::string_literals;


ConfigureOpticalTx::ConfigureOpticalTx(const std::string& aId, action::ActionableObject& aActionable) :
  Command(aId, aActionable, "Dummy command's default result!"s)
{
  registerDummyParameters(*this);
  registerParameter<std::string>({ "ids"s, "List of channel indices" }, "0-99", core::rules::NonEmptyString<std::string>());
}


ConfigureOpticalTx::~ConfigureOpticalTx()
{
}


action::Command::State ConfigureOpticalTx::code(const core::ParameterSet& aParams)
{
  // [For UI tests] Parse dummy parameters and register some dummy messages
  const auto s = readDummyParametersAndPrintMessages(aParams, [&](float x, const std::string& m) { this->setProgress(x, m); });

  // Run the configureTx method of SWATCH-independent optics controller instances
  const std::vector<size_t> lIds(utilities::parseListOfIndices(aParams.get<std::string>("ids")));
  for (auto& port : getActionable<Processor>().getOutputPorts().getPorts()) {
    auto* lOpticsController = dynamic_cast<TxPort&>(*port).getOpticsController();
    if ((std::count(lIds.begin(), lIds.end(), port->getIndex()) > 0) and port->isPresent() and lOpticsController) {
      lOpticsController->configureTx(port->getConnection()->index);

      // [For UI tests] If command will fail or TX MGT in bad health, then for consistency, tell controller
      //                to return bad monitoring data values in future for this TX channel
      const auto lTxMgtHealth = getActionable<Processor>().getController().getTxHealth(port->getIndex());
      if ((s == action::Command::kError) or (lTxMgtHealth != ComponentHealth::kError))
        lOpticsController->forceTxChannelHealth(port->getConnection()->index, ComponentHealth::kError);
    }
  }

  return s;
}


} // namespace commands
} // namespace swatch
} // namespace dummy
