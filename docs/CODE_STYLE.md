# Code style guidelines

Name conventions:
 * Variables: camel case, starting with lower case letter that denotes the scope ...
   - Local variable: `l`
   - Function parameter: `a`
   - Constant global/static variable: `k`
   - Non-constant global/static variable: `s`
   - Class/struct private member variable: `m`
   - Class/struct public member variable: no prefix
   - Examples: ```lLocalVariable, aParameterVariable, mClassMemberVariable```
 * Methods: Camel case starting with lower case. Example: ```getMeSomething();```
 * Classes and enum types: camel case starting with upper case. Example: ```MyAwesomeFritter```
 * Typedefs: Same as classes, but with `_t` suffix. Example: ```MyType_t```

Code style:
 * Indentation & braces:
   - K&R, Stroustrup variant (https://en.wikipedia.org/wiki/Indent_style#K.26R_style). Basic summary:
     - Namespaces: Opening brace on same line.
     - Functions: Opening brace on next line.
     - Blocks within functions (if, while, for, ...): Opening brace on same line as control statement.
     - Closing braces: On line of their own (e.g. ```else``` goes on a new line).
     - Statements within braces are indented, apart from namespace braces.
   - Initialisation list: Each member variable on its own line; one level of indentation.
   - Indent size: 2 spaces (NOT tab!)
 * Line wrap at 120 characters

