# Example plugin for the HERD application

This repository contains a few classes that illustrate how to create a hardware-specific/application-specific SWATCH plugin for use by an on-board control application in the phase-2 upgrades. Specifically, it contains an example device class, `DummyDevice`, that:

 * registers 5 commands, implemented by the `AlignLinks`, `ConfigureRxMGTs`, `ConfigureTxMGTs`, `Reboot` and `Reset` classes; and
 * registers an example Finite State Machine (FSM), whose transitions consist of one or more of the commands, run in sequence


## Dependencies

The main dependencies are SWATCH and the HERD application, which in turn require the following:

 * Build utilities: make, CMake3
 * [boost](https://boost.org)
 * [log4cplus](https://github.com/log4cplus/log4cplus)


## Build instructions

 1. Install the HERD app and SWATCH library, along with their dependencies, following the instructions in the app's [README](https://gitlab.cern.ch/cms-tracker-phase2-onlinesw/herd-control-app).

 2. Build the plugin:
```
mkdir build
cd build
cmake3 ..
make
```


## Using this plugin with the control application

The HERD control application (implemented in the `herd-control-app` repository) is an executable that loads SWATCH plugins, and provides a network interface that allows remote applications to run the commands and FSM transitions procedures declared in those plugins. If run with `dummy.yml` as the configuration file, the control application will load the dummy plugin and create two devices (of type `swatch::dummy::DummyDevice`, named `x0` and `x1`). This can be done in one of two ways:

 * Without building the plugin, by simply running the docker container:
```
docker run -it -p 3000:3000 gitlab-registry.cern.ch/cms-tracker-phase2-onlinesw/herd-dummy/herd-app:v0.2.0
```

 * Or, after installing the HERD control application (see build instructions above), by running:
```
source env.sh
herd-app dummy.yml
```

While the control application is running, you can control the dummy devices instantiated by this server (i.e. run their commands and FSM transitions) using `herd-control-console.py`. In this console you can:
 * list device, command and FSM information by typing `info`;
 * run commands and FSM transitions by typing `run DEVICE_ID COMMAND_OR_TRANSITION_ID` (e.g. `run x0 reset`);
 * engage FSMs by typing `engage-fsm DEVICE_ID FSM_ID` (e.g. `engage-fsm x0 myFSM`); and
 * reset FSMs by typing `reset-fsm DEVICE_ID FSM_ID` (e.g. `reset-fsm x0 myFSM`); and
 * disengage FSMs by typing `disengage-fsm DEVICE_ID FSM_ID` (e.g. `disengage-fsm x0 myFSM`); and

The console can started by running the docker container:
```
docker run -it --network=host gitlab-registry.cern.ch/cms-tracker-phase2-onlinesw/herd-control-app/console:v0.2.0
```

