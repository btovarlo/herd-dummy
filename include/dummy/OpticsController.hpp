
#ifndef __DUMMY_OPTICSCONTROLLER_HPP__
#define __DUMMY_OPTICSCONTROLLER_HPP__


#include <map>
#include <memory>
#include <stdint.h>
#include <string>
#include <unordered_map>
#include <vector>

#include "dummy/definitions.hpp"


namespace dummy {

struct OpticalModuleStatus;
struct OpticalRxChannelStatus;
struct OpticalTxChannelStatus;

class OpticsController {
private:
  //! Represents the status (good/warning/error/unreachable) of the components
  OpticsController(const std::vector<size_t>& aRxChannelIndices, const std::vector<size_t>& aTxChannelIndices);

  OpticsController(const OpticsController&) = delete;
  OpticsController& operator=(const OpticsController&) = delete;

public:
  ~OpticsController();

  ComponentHealth getModuleHealth() const;

  ComponentHealth getRxChannelHealth(size_t i) const;

  ComponentHealth getTxChannelHealth(size_t i) const;

  OpticalModuleStatus getModuleStatus() const;

  OpticalRxChannelStatus getRxChannelStatus(size_t i) const;

  OpticalTxChannelStatus getTxChannelStatus(size_t i) const;

  void powerOff();

  void powerOn();

  void reset();

  void configureRx(const size_t i);

  void configureTx(const size_t i);

  //! Dummy method that forces what range of values are returned for module-level monitoring data (good vs bad)
  void forceModuleHealth(ComponentHealth aNewState);

  //! Dummy method that forces what range of values are returned for rx channel monitoring data (good vs bad)
  void forceRxChannelHealth(size_t i, ComponentHealth aNewState);

  //! Dummy method that forces what range of values are returned for tx channel monitoring data (good vs bad)
  void forceTxChannelHealth(size_t i, ComponentHealth aNewState);

  static OpticsController* get(const std::string&);

  static OpticsController& create(const std::string&, const std::vector<size_t>& aRx, const std::vector<size_t>& aTx);

  static std::unordered_map<std::string, std::unique_ptr<OpticsController>>::const_iterator begin();

  static std::unordered_map<std::string, std::unique_ptr<OpticsController>>::const_iterator end();

private:
  ComponentHealth mModuleStatus;
  std::map<size_t, ComponentHealth> mRxChannelStatus, mTxChannelStatus;

  static std::unordered_map<std::string, std::unique_ptr<OpticsController>> sControllerMap;
};

} // namespace dummy

#endif /* __DUMMY_OPTICSCONTROLLER_HPP__ */
