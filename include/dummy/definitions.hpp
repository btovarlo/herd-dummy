
#ifndef __DUMMY_DEFINITIONS_HPP__
#define __DUMMY_DEFINITIONS_HPP__


namespace dummy {

enum class ComponentHealth {
  kGood,
  kWarning,
  kError,
  kNotReachable
};


enum class Direction {
  kRx,
  kTx
};

} // namespace dummy


#endif /* __DUMMY_DEFINITIONS_HPP__ */
