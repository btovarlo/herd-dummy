
#ifndef __DUMMY_OPTICALMODULESTATUS_HPP__
#define __DUMMY_OPTICALMODULESTATUS_HPP__


namespace dummy {

struct OpticalModuleStatus {
  float temperature { 25.0 };
};

} // namespace dummy


#endif /* __DUMMY_OPTICALMODULESTATUS_HPP__ */
