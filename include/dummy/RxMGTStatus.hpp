
#ifndef __DUMMY_RXMGTSTATUS_HPP__
#define __DUMMY_RXMGTSTATUS_HPP__


namespace dummy {

struct RxMGTStatus {
  bool isLocked { true };
  bool isAligned { true };

  uint8_t sourceChannelId { 0xFF };
  uint8_t sourceSlotId { 0xF };
  uint8_t sourceCrateId { 0xFF };

  uint16_t packetCount { 0xFFFF };
  uint16_t crcErrorCount { 0 };
  uint16_t headerErrorCount { 0 };
  uint16_t fillerErrorCount { 0 };
};

} // namespace dummy


#endif /* __DUMMY_RXMGTSTATUS_HPP__ */
