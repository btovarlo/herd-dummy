
#ifndef __DUMMY_CONTROLLER_HPP__
#define __DUMMY_CONTROLLER_HPP__


#include <map>
#include <stdint.h>
#include <string>
#include <vector>

#include "dummy/definitions.hpp"


namespace dummy {

struct TTCStatus;
struct ReadoutStatus;
struct RxMGTStatus;
struct TxMGTStatus;
struct AlgoStatus;

class Controller {
private:
  struct LinkSourceInfo {
    uint8_t channelId { 0xFF };
    uint8_t slotId { 0xF };
    uint8_t crateId { 0xFF };
  };

public:
  //! Represents the status (good/warning/error/unreachable) of the components
  Controller(const std::string& aId, const size_t aRxChannels, const size_t aTxChannels);

  ~Controller();

  TTCStatus getTTCStatus() const;

  ReadoutStatus getReadoutStatus() const;

  RxMGTStatus getRxPortStatus(uint32_t channelId) const;

  TxMGTStatus getTxPortStatus(uint32_t channelId) const;

  AlgoStatus getAlgoStatus() const;

  void powerOff();

  void powerOn();

  void program();

  void reset();

  void configureRxBuffers(const std::string& aDataFilePath);

  void configureTxBuffers(const std::string& aDataFilePath);

  std::string saveRxBuffers() const;

  std::string saveTxBuffers() const;

  void configureRxMGTs(const std::vector<size_t>&);

  void configureTxMGTs(const std::vector<size_t>&);

  void configureReadout();

  void configureAlgo();

  void alignLinks();

  ComponentHealth getTxHealth(size_t) const;

  //! Dummy method that forces what range of values are returned for TTC monitoring data (good vs bad)
  void forceClkTtcHealth(ComponentHealth aNewState);

  //! Dummy method that forces what range of values are returned for inputPort monitoring data (good vs bad)
  void forceRxPortHealth(size_t i, ComponentHealth aNewState);

  //! Dummy method that forces what range of values are returned for output port monitoring data (good vs bad)
  void forceTxPortHealth(size_t i, ComponentHealth aNewState);

  //! Dummy method that forces what range of values are returned for readout monitoring data (good vs bad)
  void forceReadoutHealth(ComponentHealth aNewState);

  //! Dummy method that forces what range of values are returned for algo monitoring data (good vs bad)
  void forceAlgoHealth(ComponentHealth aNewState);

private:
  static std::string getLinkSourceInfoFilepath();
  static std::map<size_t, LinkSourceInfo> parseLinkSourceInfoFile(const std::string& aFilePath, const std::string& aSiteName);

  const std::string mId;
  std::string mRxBufferDataPath, mTxBufferDataPath;
  ComponentHealth mClkStatus;
  std::map<size_t, ComponentHealth> mTxStatus;
  std::map<size_t, ComponentHealth> mRxStatus;
  ComponentHealth mReadoutStatus;
  ComponentHealth mAlgoStatus;

  std::map<size_t, LinkSourceInfo> mLinkSourceInfo;

  static const std::string kDefaultLinkSourceInfoFilePath;
};


} // namespace dummy

#endif /* __DUMMY_CONTROLLER_HPP__ */
