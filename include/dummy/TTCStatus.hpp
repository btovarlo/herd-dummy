
#ifndef __DUMMY_TTCSTATUS_HPP__
#define __DUMMY_TTCSTATUS_HPP__


namespace dummy {

struct TTCStatus {
  bool clock40Lock { true };
  bool clock40Stopped { false };
  bool bc0Lock { true };

  uint32_t l1ACounter { 5 };
  uint32_t bunchCounter { 12 };
  uint32_t orbitCounter { 42 };
};

} // namespace dummy


#endif /* __DUMMY_TTCSTATUS_HPP__ */
