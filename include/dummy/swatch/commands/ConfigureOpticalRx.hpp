
#ifndef __DUMMY_SWATCH_COMMANDS_CONFIGUREOPTICALRX_HPP__
#define __DUMMY_SWATCH_COMMANDS_CONFIGUREOPTICALRX_HPP__


#include "swatch/action/Command.hpp"


namespace dummy {
namespace swatch {
namespace commands {


class ConfigureOpticalRx : public ::swatch::action::Command {
public:
  ConfigureOpticalRx(const std::string& aId, ::swatch::action::ActionableObject& aActionable);
  ~ConfigureOpticalRx();

private:
  State code(const ::swatch::core::ParameterSet& aParams);
};


} // namespace commands
} // namespace swatch
} // namespace dummy

#endif /* __DUMMY_SWATCH_COMMANDS_CONFIGUREOPTICALRX_HPP__ */
