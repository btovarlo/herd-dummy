
#ifndef __DUMMY_SWATCH_COMMANDS_CONFIGUREOPTICALTX_HPP__
#define __DUMMY_SWATCH_COMMANDS_CONFIGUREOPTICALTX_HPP__


#include "swatch/action/Command.hpp"


namespace dummy {
namespace swatch {
namespace commands {


class ConfigureOpticalTx : public ::swatch::action::Command {
public:
  ConfigureOpticalTx(const std::string& aId, ::swatch::action::ActionableObject& aActionable);
  ~ConfigureOpticalTx();

private:
  State code(const ::swatch::core::ParameterSet& aParams);
};


} // namespace commands
} // namespace swatch
} // namespace dummy

#endif /* __DUMMY_SWATCH_COMMANDS_CONFIGUREOPTICALTX_HPP__ */
