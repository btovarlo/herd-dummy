
#ifndef __DUMMY_SWATCH_COMMANDS_FORCERXPORTHEALTH_HPP__
#define __DUMMY_SWATCH_COMMANDS_FORCERXPORTHEALTH_HPP__


#include "swatch/action/Command.hpp"

#include "dummy/Controller.hpp"


namespace dummy {
namespace swatch {
namespace commands {


class ForceRxPortHealth : public ::swatch::action::Command {
public:
  ForceRxPortHealth(const std::string& aId, ::swatch::action::ActionableObject& aActionable);
  ~ForceRxPortHealth();

private:
  State code(const ::swatch::core::ParameterSet& aParams);

  static const std::map<std::string, ComponentHealth> kComponentHealthMap;
};


} // namespace commands
} // namespace swatch
} // namespace dummy

#endif /* __DUMMY_SWATCH_COMMANDS_FORCERXPORTHEALTH_HPP__ */
