#ifndef __EMP_SWATCH_COMMANDS_SAMESIZECONSTRAINT__
#define __EMP_SWATCH_COMMANDS_SAMESIZECONSTRAINT__


#include "swatch/action/Command.hpp"


namespace dummy {
namespace swatch {
namespace commands {

class SameSizeConstraint : public ::swatch::core::PSetConstraint {
public:
  SameSizeConstraint(const std::vector<std::string>& aStringParameters, const std::vector<std::string>& aUIntParameters, const std::vector<std::string>& aBoolParameters);

  ~SameSizeConstraint();

  void describe(std::ostream& aStream) const;

  virtual ::swatch::core::Match verify(const ::swatch::core::ParameterSet&) const;

private:
  std::vector<std::string> mStringParameters, mUIntParameters, mBoolParameters;
};

} // namespace commands
} // namespace swatch
} // namespace emp

#endif