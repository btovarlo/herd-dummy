
#ifndef __DUMMY_SWATCH_COMMANDS_RESET_HPP__
#define __DUMMY_SWATCH_COMMANDS_RESET_HPP__


#include "swatch/action/Command.hpp"


namespace dummy {
namespace swatch {
namespace commands {


class Reset : public ::swatch::action::Command {
public:
  Reset(const std::string& aId, ::swatch::action::ActionableObject& aActionable);
  ~Reset();

private:
  State code(const ::swatch::core::ParameterSet& aParams);
};


} // namespace commands
} // namespace swatch
} // namespace dummy

#endif /* __DUMMY_SWATCH_COMMANDS_RESET_HPP__ */
