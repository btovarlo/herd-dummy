
#ifndef __DUMMY_SWATCH_COMMANDS_FORCECOMPONENTHEALTH_HPP__
#define __DUMMY_SWATCH_COMMANDS_FORCECOMPONENTHEALTH_HPP__


#include "swatch/action/Command.hpp"

#include "dummy/Controller.hpp"


namespace dummy {
namespace swatch {
namespace commands {


class ForceComponentHealth : public ::swatch::action::Command {
public:
  ForceComponentHealth(const std::string& aId, ::swatch::action::ActionableObject& aActionable);
  ~ForceComponentHealth();

private:
  State code(const ::swatch::core::ParameterSet& aParams);

  static const std::map<std::string, std::function<void(Controller*, ComponentHealth)>> kMemberFunctionMap;

  static const std::map<std::string, ComponentHealth> kComponentHealthMap;
};


} // namespace commands
} // namespace swatch
} // namespace dummy

#endif /* __DUMMY_SWATCH_COMMANDS_FORCECOMPONENTHEALTH_HPP__ */
