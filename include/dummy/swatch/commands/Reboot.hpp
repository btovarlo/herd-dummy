
#ifndef __DUMMY_SWATCH_COMMANDS_REBOOT_HPP__
#define __DUMMY_SWATCH_COMMANDS_REBOOT_HPP__


#include "swatch/action/Command.hpp"


namespace dummy {
namespace swatch {
namespace commands {


class Reboot : public ::swatch::action::Command {
public:
  Reboot(const std::string& aId, ::swatch::action::ActionableObject& aActionable);
  ~Reboot();

private:
  State code(const ::swatch::core::ParameterSet& aParams);
};


} // namespace commands
} // namespace swatch
} // namespace dummy

#endif /* __DUMMY_SWATCH_COMMANDS_REBOOT_HPP__ */
