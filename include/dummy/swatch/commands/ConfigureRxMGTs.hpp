
#ifndef __DUMMY_SWATCH_COMMANDS_CONFIGURERXMGTS_HPP__
#define __DUMMY_SWATCH_COMMANDS_CONFIGURERXMGTS_HPP__


#include "swatch/action/Command.hpp"


namespace dummy {
namespace swatch {
namespace commands {


class ConfigureRxMGTs : public ::swatch::action::Command {
public:
  ConfigureRxMGTs(const std::string& aId, ::swatch::action::ActionableObject& aActionable);
  ~ConfigureRxMGTs();

private:
  State code(const ::swatch::core::ParameterSet& aParams);
};


} // namespace commands
} // namespace swatch
} // namespace dummy

#endif /* __DUMMY_SWATCH_COMMANDS_CONFIGURERXMGTS_HPP__ */
