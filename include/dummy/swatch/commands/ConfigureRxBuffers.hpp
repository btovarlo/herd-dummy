
#ifndef __DUMMY_SWATCH_COMMANDS_CONFIGURERXBUFFERS_HPP__
#define __DUMMY_SWATCH_COMMANDS_CONFIGURERXBUFFERS_HPP__


#include "swatch/action/Command.hpp"


namespace dummy {
namespace swatch {
namespace commands {


class ConfigureRxBuffers : public ::swatch::action::Command {
public:
  ConfigureRxBuffers(const std::string& aId, ::swatch::action::ActionableObject& aActionable);
  ~ConfigureRxBuffers();

private:
  State code(const ::swatch::core::ParameterSet& aParams);
};


} // namespace commands
} // namespace swatch
} // namespace dummy

#endif /* __DUMMY_SWATCH_COMMANDS_CONFIGURERXBUFFERS_HPP__ */
