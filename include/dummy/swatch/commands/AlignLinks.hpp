
#ifndef __DUMMY_SWATCH_COMMANDS_ALIGNLINKS_HPP__
#define __DUMMY_SWATCH_COMMANDS_ALIGNLINKS_HPP__


#include "swatch/action/Command.hpp"


namespace dummy {
namespace swatch {
namespace commands {


class AlignLinks : public ::swatch::action::Command {
public:
  AlignLinks(const std::string& aId, ::swatch::action::ActionableObject& aActionable);
  ~AlignLinks();

private:
  State code(const ::swatch::core::ParameterSet& aParams);
};


} // namespace commands
} // namespace swatch
} // namespace dummy

#endif /* __DUMMY_SWATCH_COMMANDS_ALIGNLINKS_HPP__ */
