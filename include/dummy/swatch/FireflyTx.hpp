#ifndef __DUMMY_SWATCH_FIREFLYTX_HPP__
#define __DUMMY_SWATCH_FIREFLYTX_HPP__


#include "swatch/phase2/OpticalModule.hpp"


namespace dummy {

class OpticsController;

namespace swatch {

// Represents a 12-channel TX Firefly part
class FireflyTx : public ::swatch::phase2::OpticalModule {
public:
  FireflyTx(const std::string&);
  ~FireflyTx();

private:
  void retrievePropertyValues() final;

  void retrieveMetricValues() final;

  void retrieveOutputMetricValues(Channel& aChannel) final;

  void retrieveOutputPropertyValues(Channel& aChannel) final;

  OpticsController& mController;

  Property<std::string>& mPartNumber;
  Property<std::string>& mFirmwareVersion;

  SimpleMetric<float>& mMetricTemperature;

  std::map<const Channel*, Property<float>*> mTxEqualization;
  std::map<const Channel*, Property<std::string>*> mTxPolarity;

  std::unordered_map<const Channel*, SimpleMetric<bool>*> mMetricLOL;
};


} // namespace swatch
} // namespace exampleboard


#endif /* __DUMMY_SWATCH_FIREFLYTX_HPP__ */
