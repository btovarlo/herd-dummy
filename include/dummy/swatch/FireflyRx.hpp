#ifndef __DUMMY_SWATCH_FIREFLYRX_HPP__
#define __DUMMY_SWATCH_FIREFLYRX_HPP__


#include "swatch/phase2/OpticalModule.hpp"


namespace dummy {

class OpticsController;

namespace swatch {

// Represents a 12-channel RX Firefly part
class FireflyRx : public ::swatch::phase2::OpticalModule {
public:
  FireflyRx(const std::string&);
  ~FireflyRx();

private:
  void retrievePropertyValues() final;

  void retrieveMetricValues() final;

  void retrieveInputMetricValues(Channel& aChannel) final;

  void retrieveInputPropertyValues(Channel& aChannel) final;

  OpticsController& mController;

  Property<std::string>& mPartNumber;
  Property<std::string>& mFirmwareVersion;

  SimpleMetric<float>& mMetricTemperature;

  std::map<const Channel*, Property<float>*> mRxAmplitude;
  std::map<const Channel*, Property<std::string>*> mRxPolarity;

  std::unordered_map<const Channel*, SimpleMetric<float>*> mMetricPower;
  std::unordered_map<const Channel*, SimpleMetric<bool>*> mMetricLOL, mMetricLOS;
};


} // namespace swatch
} // namespace exampleboard


#endif /* __DUMMY_SWATCH_FIREFLYRX_HPP__ */
