
#ifndef __DUMMY_SWATCH_READOUT_HPP__
#define __DUMMY_SWATCH_READOUT_HPP__


#include "swatch/phase2/ReadoutInterface.hpp"


namespace dummy {

class Controller;

namespace swatch {

class Readout : public ::swatch::phase2::ReadoutInterface {
public:
  Readout(const Controller&);
  ~Readout();

private:
  void retrieveMetricValues();

  const Controller& mController;
};

} // namespace swatch
} // namespace dummy


#endif /* __DUMMY_SWATCH_READOUT_HPP__ */