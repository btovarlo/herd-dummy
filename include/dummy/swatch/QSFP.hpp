#ifndef __DUMMY_SWATCH_QSFP_HPP__
#define __DUMMY_SWATCH_QSFP_HPP__


#include "swatch/phase2/OpticalModule.hpp"


namespace dummy {

class OpticsController;

namespace swatch {

// Represents a QSFP module (4 inputs & 4 outputs)
class QSFP : public ::swatch::phase2::OpticalModule {
public:
  QSFP(const std::string&);
  ~QSFP();

private:
  void retrievePropertyValues() final;

  void retrieveMetricValues() final;

  void retrieveInputMetricValues(Channel& aChannel) final;

  void retrieveOutputMetricValues(Channel& aChannel) final;

  void retrieveInputPropertyValues(Channel& aChannel) final;

  void retrieveOutputPropertyValues(Channel& aChannel) final;

  OpticsController& mController;

  Property<std::string>& mPartNumber;
  Property<std::string>& mFirmwareVersion;

  SimpleMetric<float>& mMetricTemperature;

  std::map<const Channel*, Property<float>*> mRxAmplitude;
  std::map<const Channel*, Property<std::string>*> mRxPolarity;

  std::map<const Channel*, Property<float>*> mTxEqualization;
  std::map<const Channel*, Property<std::string>*> mTxPolarity;

  std::unordered_map<const Channel*, SimpleMetric<float>*> mMetricRxPower;
  std::unordered_map<const Channel*, SimpleMetric<bool>*> mMetricRxLOL, mMetricRxLOS;
  std::unordered_map<const Channel*, SimpleMetric<bool>*> mMetricTxLOL;
};


} // namespace swatch
} // namespace exampleboard


#endif /* __DUMMY_SWATCH_QSFP_HPP__ */
