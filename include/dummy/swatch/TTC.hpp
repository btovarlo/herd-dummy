
#ifndef __DUMMY_SWATCH_TTC_HPP__
#define __DUMMY_SWATCH_TTC_HPP__


#include "swatch/core/SimpleMetric.hpp"
#include "swatch/phase2/TTCInterface.hpp"


namespace dummy {

class Controller;

namespace swatch {

class TTC : public ::swatch::phase2::TTCInterface {
public:
  TTC(const Controller&);
  ~TTC();

private:
  static std::string createId(const size_t);

  void retrieveMetricValues();

  const Controller& mController;

  // TODO: Add any properties?
};

} // namespace swatch
} // namespace dummy


#endif /* __DUMMY_SWATCH_TTC_HPP__ */